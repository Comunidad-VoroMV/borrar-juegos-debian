Rojo='\033[31m'
Verde='\033[32m'
Blanco='\033[0m'
Turquesa='\033[36m'

echo ""
echo -e "${Turquesa}ALEJANDRO FORNER ARTAL - SEPTIEMBRE 2023"
echo ""
echo -e "${Rojo}ATENCION:${Blanco}SE ELIMINARAN TODOS LOS JUEGOS INSTALADOS"
echo -e "\033[0m"
echo ""
echo "¿ Estás seguro/a ? "
echo ""
echo "Escribe s para confirmar o n para denegar"
read lectura
if [[ $lectura == "S" || $lectura == "s" ]]; then
	sudo apt remove gnome-tetravex -y
	sudo apt remove gnome-taquin -y
	sudo apt remove gnome-sudoku -y
	sudo apt remove gnome-robots -y
	sudo apt remove gnome-nibbles -y
	sudo apt remove gnome-2048 -y
	sudo apt remove gnome-klotski -y
	sudo apt remove gnome-mines -y
	sudo apt remove gnome-chess -y
	sudo apt remove gnome-mahjongg -y
	sudo apt remove aisleriot -y
	sudo apt remove quadrapassel -y
	sudo apt remove swell-foop -y
	sudo apt remove four-in-a-row -y
	sudo apt remove hitori -y
	sudo apt remove five-or-more -y
	sudo apt remove lightsoff -y
	sudo apt remove tali -y
	sudo apt remove iagno -y
	echo ""
	echo -e " [ ${Verde}OK${Blanco} ] Se han eliminado todos los juegos"
else
	echo "PROCESO CANCELADO"
	echo ""
	echo "En 3 segundos se cerrará el programa"
	sleep 3
fi



